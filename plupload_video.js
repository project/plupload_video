(function ($) {
  $(document).ready(function () {
    var uploader = new plupload.Uploader(Drupal.settings.plupload_video);

    uploader.bind('Init', function(up, params) {
      $('#filelist').addClass(params.runtime);
    });

    $('#uploadfiles').click(function(e) {
      uploader.start();
      e.preventDefault();
    });

    uploader.init();

    uploader.bind('FilesAdded', function(up, files) {
      $.each(files, function(i, file) {
        $('#filelist').append(
          '<div class="plupload-file" id="' + file.id + '"><div>' +
          file.name + ' (' + plupload.formatSize(file.size) + ') <b></b>' +
        '</div><span></span></div>');
      });

      if (!uploader.settings.multi_selection) {
        $('#' + uploader.settings.browse_button).hide();
        // uploader.DisableBrowse;
      }

      uploader.start();
      up.refresh(); // Reposition Flash/Silverlight
    });

    uploader.bind('UploadProgress', function(up, file) {
      $('#' + file.id + ' b').html(file.percent + "%");
      $('#' + file.id + ' >span').css("width", file.percent + "%");
    });

    uploader.bind('Error', function(up, err) {
      $('#filelist').append('<div class="plupload-error">Error: ' + err.code + ': ' + err.message +
        (err.file ? ', File: ' + err.file.name : '') +
        '</div>'
      );

      up.refresh(); // Reposition Flash/Silverlight
    });

    uploader.bind('FileUploaded', function(up, file) {
      $('#' + file.id + " b").html("100%");
    });
  });
})(jQuery);
